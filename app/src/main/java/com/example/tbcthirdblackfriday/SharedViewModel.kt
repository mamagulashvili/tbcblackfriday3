package com.example.tbcthirdblackfriday

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedViewModel:ViewModel() {

    var editedUser = MutableLiveData<UserModel>()
    fun saveEditedUser(userModel: UserModel){
       editedUser.value = userModel
    }
    var user = MutableLiveData<UserModel>()
    fun sendUserInfo(userModel: UserModel){
        user.value = userModel
    }
}