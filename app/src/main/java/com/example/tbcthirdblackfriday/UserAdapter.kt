package com.example.tbcthirdblackfriday

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcthirdblackfriday.databinding.RowItemBinding

class UserAdapter(val userList: MutableList<UserModel>, val onItemListener: OnItemListener) :
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int = userList.size

    inner class UserViewHolder(val binding: RowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            val model = userList[adapterPosition]
            binding.apply {
                tvFirstName.text = model.name
                tvLastName.text = model.lastName
                tvEmail.text = model.email
                btnDelete.setOnClickListener {
                    onItemListener.userDelete(adapterPosition)
                }

            }
            binding.btnEdit.setOnClickListener {
                onItemListener.userEdit(adapterPosition)
            }
        }

    }

}