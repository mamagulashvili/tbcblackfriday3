package com.example.tbcthirdblackfriday

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserModel(
    val name:String,
    val lastName:String,
    val email:String
):Parcelable
