package com.example.tbcthirdblackfriday

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.example.tbcthirdblackfriday.databinding.FragmentUserEditBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class UserEditFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentUserEditBinding
    private val sharedViewModel: SharedViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserEditBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        binding.btnSave.setOnClickListener {
            sendEditedUserData()
            dismiss()
        }
        getData()
    }

    private fun getData() {
        sharedViewModel.user.observe(viewLifecycleOwner, Observer {
            binding.apply {
                etEmail.setText(it.email)
                etFirstName.setText(it.name)
                etLastName.setText(it.lastName)
            }
        })
    }

    private fun sendEditedUserData() {
        val email = binding.etEmail.text.toString()
        val firstName = binding.etFirstName.text.toString()
        val lastName = binding.etLastName.text.toString()
        val editedUser = UserModel(firstName, lastName, email)
        sharedViewModel.saveEditedUser(editedUser)
    }
}