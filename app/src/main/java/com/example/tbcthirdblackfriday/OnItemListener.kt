package com.example.tbcthirdblackfriday

interface OnItemListener {
    fun userDelete(position:Int)
    fun userEdit(position: Int)
}