package com.example.tbcthirdblackfriday

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tbcthirdblackfriday.databinding.FragmentUsersBinding

class UsersFragment : Fragment() {
    private lateinit var binding: FragmentUsersBinding

    private lateinit var userAdapter: UserAdapter
    private val userList = mutableListOf<UserModel>()

    private val sharedViewModel: SharedViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUsersBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        getEditedData()
        setRecycle()
    }

    private fun setRecycle() {
        val onItemClickListener = object : OnItemListener {
            override fun userDelete(position: Int) {
                deleteUser(position)
            }

            override fun userEdit(position: Int) {
                editUser(position)
            }
        }
        userAdapter = UserAdapter(userList, onItemClickListener)
        binding.rvUser.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = userAdapter
        }
        setData()
    }


    private fun editUser(position: Int) {
        val userModel = userList[position]
        sharedViewModel.sendUserInfo(userModel)
        findNavController().navigate(R.id.action_usersFragment_to_userEditDialogFragment)
        userList.removeAt(position)
        userAdapter.notifyItemRemoved(position)
    }

    private fun deleteUser(position: Int) {
        userList.removeAt(position)
        userAdapter.notifyItemRemoved(position)
    }

    private fun getEditedData() {
        sharedViewModel.editedUser.observe(viewLifecycleOwner, Observer {
            userList.add(it)
            userAdapter.notifyItemChanged(userList.size - 1)
        })
    }

    private fun setData() {
        userList.apply {
            add(UserModel("Jemali", "Tabukashvili", "jemali3@gmail.com"))
            add(UserModel("Jemali", "samardaniani", "jemali5@gmail.com"))
            add(UserModel("Jemali", "kaikacishvili", "jemali6@gmail.com"))
            add(UserModel("Jemali", "mamapapovi", "jemali7@gmail.com"))
            add(UserModel("Jemali", "Tabukashvili", "jemali8@gmail.com"))
        }
        userAdapter.notifyDataSetChanged()
    }


}